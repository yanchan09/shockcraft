# Shockcraft

PiShock integration mod for Minecraft

## Note
**Use at your own caution.** This hasn't been *thoroughly* tested and I'm not responsible for any harm caused. Shock collars could kill you.

## Setup
1. [Install Fabric](https://fabricmc.net/use/installer/) if you haven't already
2. Install [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api) and [Fabric Language Kotlin](https://www.curseforge.com/minecraft/mc-mods/fabric-language-kotlin) mods
3. Download the [latest release of the mod](https://codeberg.org/yanchan09/shockcraft/releases) and install it
4. Join the game
5. Authenticate using the `/shock setup <username> <apikey> <code>` command
6. Enable shocking with `/shock on` command

## Dependencies
- [Fabric Loader](https://fabricmc.net/use/installer/)
- [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api)
- [Fabric Language Kotlin](https://www.curseforge.com/minecraft/mc-mods/fabric-language-kotlin)

## Usage
Shockcraft can be configured with chat commands:
- `/shock` - Show current settings
- `/shock setup <username> <apikey> <code>` - Authenticate with PiShock
- `/shock <on|off>` - Enable/disable shocking (saved only for the running game session)
- `/shock <duration|intensity|cooldown> <value>` - Configure settings
