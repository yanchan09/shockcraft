package lol.omg.yan.shockcraft.mixin;

import lol.omg.yan.shockcraft.client.ShockcraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.network.packet.s2c.play.GameJoinS2CPacket;
import net.minecraft.network.packet.s2c.play.HealthUpdateS2CPacket;
import net.minecraft.network.packet.s2c.play.PlayerRespawnS2CPacket;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ClientPlayNetworkHandler.class)
class ClientPlayNetworkHandlerMixin {
    @Inject(at = @At("TAIL"), method = "onGameJoin(Lnet/minecraft/network/packet/s2c/play/GameJoinS2CPacket;)V")
    private void onGameJoin(GameJoinS2CPacket packet, CallbackInfo info) {
        ShockcraftClient.INSTANCE.onPlayerHealthUpdate(null);
    }

    @Inject(at = @At("TAIL"), method = "onPlayerRespawn(Lnet/minecraft/network/packet/s2c/play/PlayerRespawnS2CPacket;)V")
    private void onPlayerRespawn(PlayerRespawnS2CPacket packet, CallbackInfo info) {
        ShockcraftClient.INSTANCE.onPlayerHealthUpdate(null);
    }

    @Inject(at = @At("TAIL"), method = "onHealthUpdate(Lnet/minecraft/network/packet/s2c/play/HealthUpdateS2CPacket;)V")
    private void onHealthUpdate(HealthUpdateS2CPacket packet, CallbackInfo info) {
        ShockcraftClient.INSTANCE.onPlayerHealthUpdate(packet.getHealth());
    }
}