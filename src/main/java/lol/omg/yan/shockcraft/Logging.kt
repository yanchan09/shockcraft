package lol.omg.yan.shockcraft

import org.slf4j.LoggerFactory

object Logging {
    val LOGGER = LoggerFactory.getLogger("shockcraft")
}
