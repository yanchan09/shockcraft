package lol.omg.yan.shockcraft.client

import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import lol.omg.yan.shockcraft.Logging.LOGGER
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException

class ShockApi {
    private val httpClient = OkHttpClient()
    private val moshi = Moshi.Builder().build()
    private val asyncCallback = HttpCallback()

    var authInfo: SettingsStore.AuthInfo? = null

    fun shock(duration: Int, intensity: Int) {
        val apiCfg = authInfo ?: return
        val payload = ShockRequest()
        payload.Username = apiCfg.username
        payload.Apikey = apiCfg.apiKey
        payload.Code = apiCfg.code
        payload.Name = "Shockcraft"
        payload.Op = 0
        payload.Duration = duration
        payload.Intensity = intensity
        val bodyJson = moshi.adapter(ShockRequest::class.java).toJson(payload)
        val request = Request.Builder()
            .url("https://do.pishock.com/api/apioperate")
            .post(bodyJson.toRequestBody("application/json".toMediaType()))
            .build()
        httpClient.newCall(request).enqueue(asyncCallback)
    }

    private inner class HttpCallback : Callback {
        override fun onFailure(call: Call, e: IOException) {
            LOGGER.error("HTTP request failed", e)
        }

        override fun onResponse(call: Call, response: Response) {
            response.use {
                if (!response.isSuccessful) {
                    LOGGER.error("Unexpected response status: $response")
                }
            }
        }
    }

    @JsonClass(generateAdapter = true)
    internal class ShockRequest {
        var Username: String? = null
        var Apikey: String? = null
        var Name: String? = null
        var Code: String? = null
        var Op: Int? = null
        var Duration: Int? = null
        var Intensity: Int? = null
    }
}