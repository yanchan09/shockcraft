package lol.omg.yan.shockcraft.client

import com.mojang.brigadier.Command
import com.mojang.brigadier.CommandDispatcher
import com.mojang.brigadier.arguments.FloatArgumentType.floatArg
import com.mojang.brigadier.arguments.FloatArgumentType.getFloat
import com.mojang.brigadier.arguments.IntegerArgumentType.getInteger
import com.mojang.brigadier.arguments.IntegerArgumentType.integer
import com.mojang.brigadier.arguments.StringArgumentType.*
import com.mojang.brigadier.context.CommandContext
import lol.omg.yan.shockcraft.Logging.LOGGER
import net.fabricmc.api.ClientModInitializer
import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.fabricmc.fabric.api.client.command.v2.ClientCommandManager.argument
import net.fabricmc.fabric.api.client.command.v2.ClientCommandManager.literal
import net.fabricmc.fabric.api.client.command.v2.ClientCommandRegistrationCallback
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource
import net.minecraft.client.MinecraftClient
import net.minecraft.command.CommandRegistryAccess
import net.minecraft.text.Style
import net.minecraft.text.Text
import net.minecraft.util.Formatting

@Environment(EnvType.CLIENT)
class ShockcraftClient : ClientModInitializer {
    companion object {
        lateinit var INSTANCE: ShockcraftClient;
    }

    private lateinit var minecraftClient: MinecraftClient
    private var shockApi = ShockApi()
    private var lastShock: Long = 0
    private var settingsStore = SettingsStore()
    private var lastHealth: Float? = null

    override fun onInitializeClient() {
        INSTANCE = this
        minecraftClient = MinecraftClient.getInstance()
        ClientCommandRegistrationCallback.EVENT.register(CommandRegistrationListener())
        settingsStore.loadSettings()
        shockApi.authInfo = settingsStore.authInfo
    }

    fun infoCommand(context: CommandContext<FabricClientCommandSource>): Int {
        if (settingsStore.authInfo != null) {
            context.source.sendFeedback(
                Text.literal("Authenticated!")
                    .setStyle(Style.EMPTY.withFormatting(Formatting.GREEN))
            )
        } else {
            context.source.sendFeedback(
                Text.literal("Not authenticated :( - run /shock setup")
                    .setStyle(Style.EMPTY.withFormatting(Formatting.RED))
            )
        }

        val settings = arrayOf(
            Pair("Enabled", settingsStore.enabled),
            Pair("Shock duration", settingsStore.shockDuration),
            Pair("Shock intensity", settingsStore.shockIntensity),
            Pair("Shock cooldown", settingsStore.shockCooldown),
        )

        for (setting in settings) {
            context.source.sendFeedback(Text.literal("${setting.first}: ${setting.second}"))
        }

        return Command.SINGLE_SUCCESS
    }

    fun setEnabledCommand(enabled: Boolean, context: CommandContext<FabricClientCommandSource>): Int {
        settingsStore.enabled = enabled
        settingsStore.saveSettings()

        if (enabled) {
            context.source.sendFeedback(
                Text.literal("Shockcraft enabled!")
                    .setStyle(Style.EMPTY.withFormatting(Formatting.GREEN))
            )
        } else {
            context.source.sendFeedback(
                Text.literal("Shockcraft disabled!")
                    .setStyle(Style.EMPTY.withFormatting(Formatting.GREEN))
            )
        }

        return Command.SINGLE_SUCCESS
    }

    enum class Setting {
        DURATION, INTENSITY, COOLDOWN
    }

    fun setValueCommand(setting: Setting, context: CommandContext<FabricClientCommandSource>): Int {
        when (setting) {
            Setting.DURATION -> settingsStore.shockDuration = getInteger(context, "value")
            Setting.INTENSITY -> settingsStore.shockIntensity = getInteger(context, "value")
            Setting.COOLDOWN -> settingsStore.shockCooldown = getFloat(context, "value")
        }
        settingsStore.saveSettings()

        context.source.sendFeedback(
            Text.literal("Settings saved!")
                .setStyle(Style.EMPTY.withFormatting(Formatting.GREEN))
        )

        return Command.SINGLE_SUCCESS
    }

    fun setupCommand(context: CommandContext<FabricClientCommandSource>): Int {
        val username = getString(context, "username")
        val apiKey = getString(context, "apikey")
        val code = getString(context, "code")

        settingsStore.authInfo = SettingsStore.AuthInfo(username, apiKey, code)
        shockApi.authInfo = settingsStore.authInfo
        settingsStore.saveSettings()

        context.source.sendFeedback(
            Text.literal("Settings saved!")
                .setStyle(Style.EMPTY.withFormatting(Formatting.GREEN))
        )

        return Command.SINGLE_SUCCESS
    }

    private fun maybeShock() {
        if (!settingsStore.enabled) return
        val currentTime = System.currentTimeMillis()
        val cooldownMillis = settingsStore.shockDuration * 1000L + (settingsStore.shockCooldown * 1000).toLong()
        if (currentTime >= lastShock + cooldownMillis) {
            shockApi.shock(settingsStore.shockDuration, settingsStore.shockIntensity)
            lastShock = currentTime
        }
    }

    fun onPlayerHealthUpdate(newHealth: Float?) {
        val lastHealth = this.lastHealth
        LOGGER.debug("onPlayerHealthUpdate: lastHealth = $lastHealth, newHealth = $newHealth")
        if (newHealth != null && lastHealth != null) {
            val damage = lastHealth - newHealth
            if (damage > 0) {
                LOGGER.info("We got $damage of damage")
                maybeShock()
            }
        }
        this.lastHealth = newHealth
    }

    inner class CommandRegistrationListener : ClientCommandRegistrationCallback {
        override fun register(
            dispatcher: CommandDispatcher<FabricClientCommandSource>?,
            registryAccess: CommandRegistryAccess?
        ) {
            val enableCommand = literal("on").executes { context -> setEnabledCommand(true, context) }
            val disableCommand = literal("off").executes { context -> setEnabledCommand(false, context) }
            val setDurationCommand = literal("duration")
                .then(argument("value", integer(1, 15))
                    .executes { context -> setValueCommand(Setting.DURATION, context) })
            val setIntensityCommand = literal("intensity")
                .then(argument("value", integer(1, 100))
                    .executes { context -> setValueCommand(Setting.INTENSITY, context) })
            val setCooldownCommand = literal("cooldown")
                .then(argument("value", floatArg(0f))
                    .executes { context -> setValueCommand(Setting.COOLDOWN, context) })
            val setupCommand = literal("setup")
                .then(
                    argument("username", string())
                        .then(
                            argument("apikey", word())
                                .then(argument("code", word())
                                    .executes { context -> setupCommand(context) })
                        )
                )
            val rootCommand = literal("shock")
                .then(enableCommand)
                .then(disableCommand)
                .then(setupCommand)
                .then(setDurationCommand)
                .then(setIntensityCommand)
                .then(setCooldownCommand)
                .executes { context -> infoCommand(context) }
            dispatcher?.register(rootCommand)
        }

    }
}