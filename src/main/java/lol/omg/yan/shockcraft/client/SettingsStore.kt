package lol.omg.yan.shockcraft.client

import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import lol.omg.yan.shockcraft.Logging.LOGGER
import java.io.File

class SettingsStore {
    private val moshi = Moshi.Builder().build()

    var enabled = false
    var authInfo: AuthInfo? = null
    var shockDuration = 1
    var shockIntensity = 5
    var shockCooldown = 5f

    @JsonClass(generateAdapter = true)
    data class AuthInfo(val username: String, val apiKey: String, val code: String)

    @JsonClass(generateAdapter = true)
    data class SavedSettings(
        val authInfo: AuthInfo?,
        val shockDuration: Int,
        val shockIntensity: Int,
        val shockCooldown: Float
    )

    fun loadSettings() {
        try {
            val text = File("./shockcraft.json").readText()
            val settings = moshi.adapter(SavedSettings::class.java).fromJson(text) ?: return
            this.authInfo = settings.authInfo
            this.shockDuration = settings.shockDuration
            this.shockIntensity = settings.shockIntensity
            this.shockCooldown = settings.shockCooldown
        } catch (e: Exception) {
            LOGGER.error("Failed to load settings", e)
        }
    }

    fun saveSettings() {
        try {
            val settings = SavedSettings(authInfo, shockDuration, shockIntensity, shockCooldown)
            val jsonText = moshi.adapter(SavedSettings::class.java).toJson(settings)
            File("./shockcraft.json").writeText(jsonText)
        } catch (e: Exception) {
            LOGGER.error("Failed to save settings", e)
        }
    }
}